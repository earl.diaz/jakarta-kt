package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//	Servlets have a Lifecycle
	//	There is the initialization which serves as the first stage of a servlet
	public void init() throws ServletException {
		System.out.println("*******************************");
		System.out.println("Initialized Calculator Servlet!");
		System.out.println("*******************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from Calculator Servlet!");
		
		int firstNumber = Integer.parseInt(req.getParameter("num1"));
		int secondNumber = Integer.parseInt(req.getParameter("num2"));
		
		int total = firstNumber + secondNumber;
		
		PrintWriter out = res.getWriter();
		
		out.println("The total of the two numbers is " + total);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		
		out.println("This is the GET Method.");
	}
	
	public void destroy() {
		System.out.println("*******************************");
		System.out.println("Finalized Calculator Servlet!");
		System.out.println("*******************************");
	}
}
