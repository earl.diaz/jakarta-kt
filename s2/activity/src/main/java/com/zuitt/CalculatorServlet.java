package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void init() {
		System.out.println("*********************");
		System.out.println("Servlet Initialized!");
		System.out.println("*********************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {	
		PrintWriter out = res.getWriter();
		
		out.println("<h1>You are now using the Calculator app!</h1>");
		out.println("<p>To use the app, input two numbers and an operation.</p>");
		out.println("<p>Hit the submit button after filling the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		int firstNumber = Integer.parseInt(req.getParameter("num1"));
		int secondNumber = Integer.parseInt(req.getParameter("num2"));
		int total;
		PrintWriter out = res.getWriter();
		
		if(req.getParameter("operation").equalsIgnoreCase("add")) {
			total = firstNumber + secondNumber;
			
			out.println("The total of the two numbers is " + total);
		} else if (req.getParameter("operation").equalsIgnoreCase("subtract")) {
			total = firstNumber - secondNumber;
			
			out.println("The difference of the two numbers is " + total);
		} else if (req.getParameter("operation").equalsIgnoreCase("multiply")) {
			total = firstNumber * secondNumber;
			
			out.println("The product of the two numbers is " + total);
		} else if (req.getParameter("operation").equalsIgnoreCase("divide")) {
			total = firstNumber / secondNumber;
			
			out.println("The quotient of the two numbers is " + total);
		} else {
			out.println("Please input a valid math operation.");
		}
	}
	
	public void destroy() {
		System.out.println("*********************");
		System.out.println("Servlet Finalized!");
		System.out.println("*********************");
	}
}
