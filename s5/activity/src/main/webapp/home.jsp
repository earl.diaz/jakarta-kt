<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Welcome Page</title>
	</head>
	<body>
		<%
			String fullName = session.getAttribute("fullname").toString();
			String message = session.getAttribute("message").toString();
			System.out.println(fullName);
		%>
		
		<h1>Welcome to the Job Application</h1>
		<p>Hello user, <%= fullName %>!</p>
		<p><%= message %></p>
	</body>
</html>