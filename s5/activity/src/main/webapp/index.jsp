<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Registration Form</title>
		<style type="text/css">
			div{
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container{
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
					
			input[type=text], input[type=email], input[type=date], select, textarea{
				width: 100%;
  				clear: both;
			}
		</style>
	</head>
	<body>
		<div id="container">
			<h1>Registration Form</h1>
			<form action="register" method="post">
				<div>
					<label for="lastname">First Name: </label>
					<input type="text" name="firstname" id="firstname" />
				</div>
				
				<div>
					<label for="lastname">Last Name: </label>
					<input type="text" name="lastname" id="lastname" />
				</div>
				
				<div>
					<label for="phone">Phone Number: </label>
					<input type="text" name="phone" id="phone" />
				</div>
				
				<div>
					<label for="email">Email Address: </label>
					<input type="email" name="email" id="email" />
				</div>
				
				<div>
					<label for="account_type">Account Type: </label>
					<select id="account_type" name="account_type">
						<option value="" selected="selected">Select One</option>
						<option value="employer">Employer</option>
						<option value="applicant">Applicant</option>
					</select>
				</div>
				
				<fieldset>
					<legend>App Discovery</legend>
			
					<input type="radio" id="friends" name="app_discovery" required value="friends" />
					<label for="friends">Friends</label>
					
					<input type="radio" id="social_media" name="app_discovery" required value="social_media" />
					<label for="social_media">Social Media</label>
					
					<input type="radio" id="others" name="app_discovery" required value="others" />
					<label for="others">Others</label>
				</fieldset>
				
				<!-- Date of Birth -->
				<div>
					<label for="date_of_birth">Date of Birth:</label>
					<input type="date" name="date_of_birth" required>
				</div>
				
				<!-- Comment box -->
				<div>
					<label for="comments">Profile Description: </label>
					<br>
					<textarea name="comments" maxlength="500"></textarea>
				</div>
				
				<!-- Submit -->
				<button>Register</button>
			</form>
		</div>
	</body>
</html>