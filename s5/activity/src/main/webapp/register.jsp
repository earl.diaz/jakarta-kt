<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    
<%@ include file="accountType.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Register Confirmation</title>
	</head>
	<body>
		<h1>Registration Confirmation</h1>
		<p>First Name: <%= session.getAttribute("firstname") %></p>
		<p>Last Name: <%= session.getAttribute("lastname") %></p>
		<p>Phone Number: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>Account Type: <%= accountType %></p>
		<p>App Discovery: <%= session.getAttribute("appDiscovery") %></p>
		<p>Comments: <%= session.getAttribute("comments") %></p>
		
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
	</body>
</html>