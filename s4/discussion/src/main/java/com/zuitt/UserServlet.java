package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

//@WebServlet annotation will allow us to map our servlet into an endpoint without web.xml
@WebServlet(urlPatterns="/user") // initParams can be passed by annotations: initParams={@WebInitArams(name="hello", value="hi"}
public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() {
		System.out.println("********************************");
		System.out.println("UserServlet has been initialized");
		System.out.println("********************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// Capture data from form input:
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
//		PrintWriter out = res.getWriter();
//		
//		out.println(
//				"<h1>User Details</h1>" +
//				"<p>First Name: " + firstName + "</p>" +
//				"<p>Last Name: " + lastName + "</p>" +
//				"<p>Email: " + email + "</p>" +
//				"<p>Contact: " + contact + "</p>"
//				);
		
		//Pass the captured data in our servlet context to be consumed later by our database servlet
		
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("firstName", firstName);
		srvContext.setAttribute("lastName", lastName);
		srvContext.setAttribute("email", email);
		srvContext.setAttribute("contact", contact);
		
		//redirect to next servlet
		res.sendRedirect("database");
	}
	
	public void destroy() {
		System.out.println("********************************");
		System.out.println("UserServlet has been finalized");
		System.out.println("********************************");
	}
}
