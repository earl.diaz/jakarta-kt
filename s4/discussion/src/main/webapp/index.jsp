<!-- Directives are defined by "%@" symbol to define the page attributes -->
<!-- Directives are also what we use to import packages, class in our JSP -->
<%@ 
	page language="java"
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.Date"
%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<title>JSP</title>
	</head>
	<body>
		<h1>Welcome to the Hotel Servlet</h1>
		<p>You can check out any time you like, but you can never leave.</p>
		
		<!-- JSP allows integration of HTML tags with Java syntax -->
		<!-- "JSP Scriptlets" will allow us to add variables and execute java code. -->
		<% System.out.println("Hello from JSP"); %>
		<h2>The time now is: </h2>
		<% 
			Date currentDateTime = new java.util.Date();
			String name = "Earl";
			String address = "BGC";
			int age = 20;
		
		%>
		<!-- "JSP Expression" tags will allow us to print the value of embedded/declared variables as string into an html element -->
		<p><%= currentDateTime %></p>
		<h4>My Details</h4>
		<p>My name is <%=name %></p>
		<p>I live in <%=address %></p>
		<p>I am <%=age %> years old</p>
		
		<!-- "JSP Declaration" tags can also be used to create variables and methods: -->
		<%!
			private int initVar = 0;
			private int serviceVar = 0;
			private int destroyVar = 0;
			
			// Define the init and destroy methods of a jsp
			// In jsp, the init and destroy methods are named as jspInit and jspDestroy
			
			public void jspInit(){
				initVar++;
				System.out.println("jspInit(): init " +initVar);
			}
			
			public void jspDestroy(){
				destroyVar++;
				System.out.println("jspDestroy(): destroy " +destroyVar);
			}
		%>
		
		<!-- JSP scriptlets declarations and definitions are actually added into the internal jspService() method -->
		<!-- Meanwhile, JSP declaration allows us to create variables and methods at a class level meaning it is added outside the _jspService() methods -->
		<%
			System.out.println(age); //added into the _jspService method
			serviceVar++;
			//Therefore everytime the _jsepService() methods is used, serviceVar will increment
			
			String content1 = "content1: " + initVar;
			String content2 = "content2: " + serviceVar;
			String content3 = "content3: " + destroyVar;
		%>
		
		<h1>JSP Expressions</h1>
		<p><%= content1 %></p>
		<p><%= content2 %></p>
		<p><%= content3 %></p>
		
		<br>
		
		<h1>Create an Account</h1>
		<form action="user" method="post">
			<label for="firstName">First Name:</label>
			<br>
			<input type="text" name="firstName" id="firstname">
			<br>
			<label for="lastName">Last Name:</label>
			<br>
			<input type="text" name="lastName" id="lastName">
			<br>
			<label for="email">Email:</label>
			<br>
			<input type="email" name="email" id="email">
			<br>
			<label for="contact">Contact:</label>
			<br>
			<input type="text" name="contact" id="contact">
			<br>
			<input type="submit"/>
		</form>
	</body>
</html>