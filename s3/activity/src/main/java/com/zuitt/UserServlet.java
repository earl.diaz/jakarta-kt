package com.zuitt;

import java.io.IOException;
import java.util.Properties;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException {
		System.out.println("******************");
		System.out.println("UserServlet has been initialized");
		System.out.println("******************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// First Name in System Properties	
		Properties properties = System.getProperties();
		properties.put("firstName", req.getParameter("firstname"));
		System.setProperties(properties);
		
		// Last Name in HTTP session
		HttpSession session = req.getSession();
		session.setAttribute("lastName", req.getParameter("lastname"));
		
		// Email in Context
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", req.getParameter("email"));
		
		// Contact Number in URL
		res.sendRedirect("details?contact="+req.getParameter("contact"));
	}
	
	public void destroy() {
		System.out.println("******************");
		System.out.println("UserServlet has been finalized");
		System.out.println("******************");
	}
}
