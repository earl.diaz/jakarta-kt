package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException {
		System.out.println("******************");
		System.out.println("DetailsServlet has been initialized");
		System.out.println("******************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		HttpSession session = req.getSession();
		ServletContext srvContext = getServletContext();
		
		out.println("<h1>Welcome to Phonebook</h1>");
		out.println("<p>First Name: " + System.getProperty("firstName") + "</p>");
		out.println("<p>Last Name: " + session.getAttribute("lastName") + "</p>");
		out.println("<p>Contact No.: " + req.getParameter("contact") + "</p>");
		out.println("<p>Email: " + srvContext.getAttribute("email") + "</p>");
	}
	
	public void destroy() {
		System.out.println("******************");
		System.out.println("DetailsServlet has been finalized");
		System.out.println("******************");
	}
}
