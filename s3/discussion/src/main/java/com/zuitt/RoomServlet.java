package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class RoomServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7802515668491130811L;
	
	//database placeholder for storing information
	private ArrayList<String> data = new ArrayList<>();
	
	public void init() throws ServletException {
		System.out.println("******************");
		System.out.println("RoomServlet has been initialized");
		System.out.println("******************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		//Get the System class pass data as its property:
//		System.getProperties().put("facilities", "Swimming Pool, Gym, Grand Ballroom, Offices");
		
		//Retrieve System properties
//		String facilities = System.getProperty("facilities");
		
//		PrintWriter out = res.getWriter();
//		
//		out.println(facilities);
		
		//Send information via the HttpSession
		HttpSession session = req.getSession();
		session.setAttribute("availableRooms", "standard");
		
		String facilities = "Swimming Pool, Gym, Grand Ballroom, Offices";
		
		//SendRedirect will allow us to redirect to another servlet along with our query string:
		res.sendRedirect("information?facilities="+facilities);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		String roomName = req.getParameter("roomtype");
		
		data.add(roomName);
		
		// To be able to add to the servlet context that is not initialized, we use setAttribute:
		ServletContext srvContext = getServletContext();
		
		//add the arraylist in our servlet context.
		srvContext.setAttribute("data", data);
		
		//redirect your request to another servlet along the request and response object.
		RequestDispatcher rd = req.getRequestDispatcher("information");
		rd.forward(req, res);

		PrintWriter out = res.getWriter();
		
		out.println(data);
		
	}
	
	public void destroy() {
		System.out.println("******************");
		System.out.println("RoomServlet has been finalized");
		System.out.println("******************");
	}

}
